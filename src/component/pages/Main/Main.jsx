import React from "react";
import Footer from "../../Footer/Footer";
import { useState, useEffect } from "react";
import { Header } from "../../Header/Header";
import { Outlet } from "react-router-dom";

const likeStorage = JSON.parse(localStorage.getItem("like"));
const basketStorage = JSON.parse(localStorage.getItem("basket"));

function Main() {
  const [like, setLike] = useState(likeStorage || []);
  const [basket, setBasket] = useState(basketStorage || {});
  const [article, setArticle] = useState([]);
  const [products, setProducts] = useState([]);
  const [isModalVisibleFirst, setIsModalVisibleFirst] = useState(false);
  const [isModalVisibleSecond, setIsModalVisibleSecond] = useState(false);

  useEffect(() => {
    fetch("./product/data/data.json")
      .then((r) => r.json())
      .then((data) => {
        setProducts(data);
      });
  }, []);

  useEffect(() => {
    localStorage.setItem("like", JSON.stringify(like));
  }, [like]);

  useEffect(() => {
    localStorage.setItem("basket", JSON.stringify(basket));
  });
  function sumSalaries(salaries) {
    let sum = 0;
    for (let salary of Object.values(basket)) {
      sum += salary;
    }

    return sum;
  }

  return (
    <div>
      <Header length={like.length} basket={sumSalaries()} />
      <Outlet
        context={{
          like,
          setLike,
          basket,
          setBasket,
          article,
          setArticle,
          products,
          setProducts,
          isModalVisibleFirst,
          setIsModalVisibleFirst,
          isModalVisibleSecond,
          setIsModalVisibleSecond,
        }}
      />
      <Footer />
    </div>
  );
}

export default Main;
