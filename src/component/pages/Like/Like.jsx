import React from "react";
import { Card } from "../../Card/Card";
import { useOutletContext } from "react-router-dom";
import { Button } from "../../Button/Button";
import "./Like.scss";

function Like() {
  const { products, like, setLike } = useOutletContext();
  const productList = products.filter((products) =>
    like.includes(products.article)
  );
  const removeLike = (prodId) => {
    setLike((state) => state.filter((n) => n !== prodId));
  };
  return (
    <>
      {like.length === 0 ? (
        <h2>Ви не обрали улюблені товари</h2>
      ) : (
        <ul className="like">
          {productList.map((product) => (
            <Card
              product={product}
              key={product.article}
              name={product.name}
              img={product.img}
              prise={product.prise}
              color={product.color}
              article={product.article}
              action={
                <Button
                  text="Видалити з улюблених"
                  handleClick={() => removeLike(product.article)}
                />
              }
            />
          ))}
        </ul>
      )}
    </>
  );
}

export default Like;
